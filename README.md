# Space Invader Detector

## Summary
The Space Invader Detector application is designed to be an enhancement module for a 
radar in which it is capable of identifying known objects even if there is interference 
causing noise in the radar signal.

The objects are to be identified even if they are overlapping each other or if they 
are covered by the edges of the radar image.  

## Build the project
The application can be built with issuing the *mvn (clean) install* command in the main 
project folder provided that maven 3+ is installed on the machine.

## Run the application
The generated artifact can be run with *java -jar space-invader-detector-1.0-SNAPSHOT.jar* if JRE
is installed.

## Use the application
The application can be interacted with from a console window.

After the application starts up, it will show you the currently known invaders and will ask 
you to define the precision of the detector.

After pressing enter, the console will return the number of possible invaders within the 
radar, exit or give you further notifications depending on the input you provided.

*TIP:* start around 70% to get similar result to what the human eye can recognise

## How the application finds the invaders
The application simulates a selector window (a matrix) that starting from the top left 
corner of the radar image, selects a number of columns and rows and matches those to 
another selector window that starts from the bottom right edge of the invader image.

The selector window goes through every row and column from left to right, top to bottom 
until it reaches the bottom right corner of the radar image and the top left corner 
of the invader image.

The selector window size grows as it goes away from the edges and shrinks as it approaches 
them and its size never exceeds the size of the invader image.

Taking the requested percentage, the application decides if the given window is a match 
or not, and if it is, then it increases a match counter.

## Planned further improvements out of scope at this point
* return a cleaned up radar image
* introduce and generalise matrix and sub-matrix objects which are capable of storing
their original coordinates and other data required for a clean radar image
* read invader and radar images from files
* refactor with Spring Boot (no app-server setup) for the sake of DI, AOP (Exception handling, logging) and 
clean testing
* move to web micro-service architecture, replace console with API and simple html site to manage
user interactions - MVC
* ......

