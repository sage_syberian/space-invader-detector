package com.zsoltfulop.assets;

public class SpaceInvaders {

    // TODO refactor to pick invaders up from file
    // TODO generalise into RadarImage
    private final static char[][] INVADER_RADAR_IMAGE_A = new char[][]{
            "--o-----o--".toCharArray(),
            "---o---o---".toCharArray(),
            "--ooooooo--".toCharArray(),
            "-oo-ooo-oo-".toCharArray(),
            "ooooooooooo".toCharArray(),
            "o-ooooooo-o".toCharArray(),
            "o-o-----o-o".toCharArray(),
            "---oo-oo---".toCharArray()
    };

    private final static char[][] INVADER_RADAR_IMAGE_B = new char[][]{
            "---oo---".toCharArray(),
            "--oooo--".toCharArray(),
            "-oooooo-".toCharArray(),
            "oo-oo-oo".toCharArray(),
            "oooooooo".toCharArray(),
            "--o--o--".toCharArray(),
            "-o-oo-o-".toCharArray(),
            "o-o--o-o".toCharArray()
    };

    public static char[][] getInvaderRadarImageA() {
        return INVADER_RADAR_IMAGE_A;
    }

    public static char[][] getInvaderRadarImageB() {
        return INVADER_RADAR_IMAGE_B;
    }
}
