package com.zsoltfulop.tools;

import java.util.List;

/**
 * Detects how many possible distorted sub-matrices may be found within the matrix at the defined percentage.
 */
public class SubMatrixDetector {

    /**
     * Operates the flow required for getting the number of possible sub-matrices within the matrix, then calculates
     * and returns the final result.
     *
     * @param matrix     the matrix in which the subMatrix is to be detected
     * @param subMatrix  the sub-matrix to be detected
     * @param percentage the requested percentage of detection precision. Match percentage greater than or equal to this
     *                   value counts as a match
     * @return returns the probable number of sub-matrices found in the matrix at the given percentage
     */
    public static int detectSubMatrix(char[][] matrix, char[][] subMatrix, int percentage) {
        int matches = 0;
        float requestedPrecision = (float) percentage / (float) 100;
        float maxCharMatch = MatrixComparator.calculateMaxCharMatch(subMatrix);

        StepIterator stepIterator = new StepIterator();
        List<char[][]> pairedSelectorWindowList = stepIterator.iterate(matrix, subMatrix);

        char[][] matrixColSelectorWindow;
        char[][] subMatrixColSelectorWindow;
        for (int i = 1; i < pairedSelectorWindowList.size(); i++) {
            if (i % 2 != 0) {
                matrixColSelectorWindow = pairedSelectorWindowList.get(i - 1);
                subMatrixColSelectorWindow = pairedSelectorWindowList.get(i);
                if (MatrixComparator.measureSimilarity(matrixColSelectorWindow, subMatrixColSelectorWindow,
                        requestedPrecision, maxCharMatch)) {
                    matches++;
                }
            }
        }

//        System.out.println(matches);

        return matches;
    }
}