package com.zsoltfulop.tools;

/**
 * Calculates the variables required to compare two matrices and their percentage of similarity.
 */
class MatrixComparator {

    /**
     * Compares current selection taken from the matrix and the sub-matrix and returns true if their identity is above
     * the defined precision.
     *
     * @param matrixWindow       current selection of the matrix, compared with subMatrixWindow
     * @param subMatrixWindow    current selection of the subMatrix, compared with matrixWindow
     * @param requestedPrecision compared with the ratio of found matches and the maxCharMatch param
     * @param maxCharMatch       maximum possible character matches, used for calculating similarity
     * @return true if the ratio of the calculated match and maxCharMatch is equal of greater than the requestedPrecision
     */
    static boolean measureSimilarity(char[][] matrixWindow, char[][] subMatrixWindow, float requestedPrecision,
                                     float maxCharMatch) {
        float charMatch = 0;
        for (int i = 0; i < matrixWindow.length; i++) {
            for (int j = 0; j < matrixWindow[i].length; j++) {
                if (matrixWindow[i][j] == subMatrixWindow[i][j] && subMatrixWindow[i][j] == 'o') {
                    charMatch++;
                }
            }
        }
        return charMatch / maxCharMatch >= requestedPrecision;
    }

    /**
     * Calculates the maximum possible character matches within a selector window.
     *
     * @param subMatrix characters that count as matches are selected from this
     * @return maximum possible matches used for percentage calculation when measuring similarity
     */
    static float calculateMaxCharMatch(char[][] subMatrix) {
        float maxCharMatch = 0;

        for (char[] subMatrixRow : subMatrix) {
            for (char c : subMatrixRow) {
                // Space Invader specific character that forms their shape. Only this counts as a match for
                // higher precision.
                if (c == 'o') {
                    maxCharMatch++;
                }
            }
        }
        return maxCharMatch;
    }
}
