package com.zsoltfulop.tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Iterates through a matrix and a sub-matrix while selecting x*rows and y*columns from them. The size of x&y
 * selections starts from 1x1 and it gets incremented and decremented in accordance with the distance of the first and
 * last row and column from the current selection. The size of the selection never exceeds the size of the sub-matrix
 * and at the end of the iteration it is 1x1 again. The direction of the sub-matrix iteration is opposite
 * to that of the matrix iteration.
 */
class StepIterator {

    private List<char[][]> pairedSelectorWindowList = new ArrayList<char[][]>();

    /**
     * Initiates iteration to get the selector window list.
     *
     * @param matrix    the matrix in which the subMatrix should be identified
     * @param subMatrix the sub-matrix to be identified in the matrix
     * @return returns a list of matrices where every first matrix is the matrix and every second
     * matrix is the sub-matrix
     */
    List<char[][]> iterate(char[][] matrix, char[][] subMatrix) {
// TODO refactor to call stepIterateCol too and extract pairedSelectorWindowList creation
        stepIterateRow(matrix, subMatrix);

        return pairedSelectorWindowList;
    }

    /**
     * Selects the smallest possible row range from the matrix, then extends it until its row length is equal with
     * the row length of the sub-matrix. When the selection reaches the maximum possible range, the index of the first
     * and last indices of the selected range get incremented until the highest numbered index reaches the maximum row
     * length of the matrix. From that point, only the smallest index is incremented until it reaches the char with the
     * highest index within the matrix. It does the same with the sub-matrix but inverted.
     *
     * @param matrix    defines the maximum number of rows for iteration
     * @param subMatrix defines the maximum number of rows for selection
     */
    private void stepIterateRow(char[][] matrix, char[][] subMatrix) {
        int fromMatrixRow = 0, toMatrixRow = 0;
        int fromSubMatrixRow = subMatrix.length, toSubMatrixRow = subMatrix.length;

        for (int i = 0; i < matrix.length + subMatrix.length - 1; i++) {
            if (i >= subMatrix.length) {
                fromMatrixRow++;
            }
            if (i < matrix.length) {
                toMatrixRow++;
            }
            if (i < subMatrix.length) {
                fromSubMatrixRow--;
            }
            if (i >= matrix.length) {
                toSubMatrixRow--;
            }

//            System.out.print("\n\nMRR: " + fromMatrixRow + " - " + (toMatrixRow - 1 +
//                    " : SMRR: " + fromSubMatrixRow + " - " + (toSubMatrixRow - 1) + "\n==="));

            char[][] matrixRowSelectorWindow = new char[toMatrixRow - fromMatrixRow][matrix[0].length];
            char[][] subMatrixRowSelectorWindow = new char[toSubMatrixRow - fromSubMatrixRow][subMatrix[0].length];

//            System.out.println("MSW Rl: " + matrixRowSelectorWindow.length + " Cl: " + matrixRowSelectorWindow[0].length +
//                    " SMSW Rl: " + subMatrixRowSelectorWindow.length + " Cl: " + subMatrixRowSelectorWindow[0].length);

            int matrixRowSelectorWindowIndex = 0;
            int subMatrixRowSelectorWindowIndex = subMatrixRowSelectorWindow.length - 1;
            for (int j = fromMatrixRow, k = toSubMatrixRow - 1; j < toMatrixRow && k >= fromSubMatrixRow;
                 j++, k--) {

//                System.out.print("\n\n MR: " + j + " ; " + "SMR: " + k + "\n --");

                matrixRowSelectorWindow[matrixRowSelectorWindowIndex] = matrix[j];
                subMatrixRowSelectorWindow[subMatrixRowSelectorWindowIndex] = subMatrix[k];

                matrixRowSelectorWindowIndex++;
                subMatrixRowSelectorWindowIndex--;
            }

//            MatrixPrinter.printMatrix(matrixRowSelectorWindow);
//            System.out.println();
//            MatrixPrinter.printMatrix(subMatrixRowSelectorWindow);
//            System.out.println();

            stepIterateCol(matrixRowSelectorWindow, subMatrixRowSelectorWindow);
        }
    }

    /**
     * Selects all the possible column ranges between column length 1 and the maximum defined by the
     * column length of the recieved selector windows and in all possible positions. The selection is added to the
     * pairedSelectorWindowList - every first item is the matrix, every second one is the sub-matrix selector.
     *
     * @param matrixRowSelectorWindow    defines the maximum number of rows for iteration
     * @param subMatrixRowSelectorWindow defines the maximum number of rows for selection
     */
    private void stepIterateCol(char[][] matrixRowSelectorWindow, char[][] subMatrixRowSelectorWindow) {
        int range = subMatrixRowSelectorWindow[0].length;
        int matrixLength = matrixRowSelectorWindow[0].length, fromMatrixCol = 0, toMatrixCol = 0;
        int subMatrixLength = subMatrixRowSelectorWindow[0].length, fromSubMatrixCol = subMatrixLength,
                toSubMatrixCol = subMatrixLength;

        for (int i = 0; i < matrixLength + range - 1; i++) {
            if (i >= range && i < matrixLength + range - 1) {
                fromMatrixCol++;
            }
            if (i < matrixLength) {
                toMatrixCol++;
            }
            if (i < range) {
                fromSubMatrixCol--;
            }
            if (i > matrixLength - 1) {
                toSubMatrixCol--;
            }

            char[][] matrixColSelectorWindow = new char[matrixRowSelectorWindow.length][];
            char[][] subMatrixColSelectorWindow = new char[subMatrixRowSelectorWindow.length][];

            int matrixRowSelectorWindowIndex = 0;
            int subMatrixRowSelectorWindowIndex = 0;
            for (int j = 0, k = 0; j < matrixRowSelectorWindow.length && k < subMatrixRowSelectorWindow.length;
                 j++, k++) {
                matrixColSelectorWindow[j] = Arrays.copyOfRange(matrixRowSelectorWindow[matrixRowSelectorWindowIndex],
                        fromMatrixCol, toMatrixCol);

//            System.out.print("\n MCR " + fromMatrixCol + " - " + (toMatrixCol - 1) + ": ");
//            MatrixPrinter.printMatrixRow(currentMatrixColRange);

                subMatrixColSelectorWindow[k] = Arrays.copyOfRange(
                        subMatrixRowSelectorWindow[subMatrixRowSelectorWindowIndex], fromSubMatrixCol, toSubMatrixCol);

                matrixRowSelectorWindowIndex++;
                subMatrixRowSelectorWindowIndex++;
            }

//            MatrixPrinter.printMatrix(matrixColSelectorWindow);
//            System.out.println();
//            MatrixPrinter.printMatrix(subMatrixColSelectorWindow);
//            System.out.println();

            pairedSelectorWindowList.add(matrixColSelectorWindow);
            pairedSelectorWindowList.add(subMatrixColSelectorWindow);
        }
    }
}
