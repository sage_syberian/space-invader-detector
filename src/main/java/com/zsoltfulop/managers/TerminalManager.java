package com.zsoltfulop.managers;

import com.zsoltfulop.assets.RadarImage;
import com.zsoltfulop.assets.SpaceInvaders;
import com.zsoltfulop.tools.SubMatrixDetector;
import com.zsoltfulop.utils.MatrixPrinter;

import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Manages user interactions and triggers the relevant methods and generates the responses in accordance.
 */
public class TerminalManager {

    /**
     * Initiates an interactive console environment.
     */
    public static void start() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("\nRadar image with interference noise:\n");
        MatrixPrinter.printMatrix(RadarImage.getNoisyRadarImage());

        System.out.println("\nType A invaders:\n");
        MatrixPrinter.printMatrix(SpaceInvaders.getInvaderRadarImageA());
        System.out.println("\nType B invaders:\n");
        MatrixPrinter.printMatrix(SpaceInvaders.getInvaderRadarImageB());

        System.out.println("\nWelcome to the Space Invader Detector radar enhancement module. You may use it to find " +
                "the probable number of Space Invaders by properly calibrating the detection precision of the " +
                "module.\n");

        String input = null;
        while (!"exit".equals(input)) {
            System.out.println("Please enter an integer between 1 & 100 to detect Space Invaders at least with " +
                    "that match percentage. You may type \"exit\" and hit enter to shut down the detector: ");
            try {
                input = scanner.nextLine();
                if ("exit".equals(input)) {
                    System.out.println("Thank you for using the radar enhancement module. See you next time!");
                } else if (input != null && Integer.valueOf(input) >= 0 && Integer.valueOf(input) <= 100) {
                    int typeAMatches = SubMatrixDetector.detectSubMatrix(RadarImage.getNoisyRadarImage(),
                            SpaceInvaders.getInvaderRadarImageA(), Integer.valueOf(input));
                    int typeBMatches = SubMatrixDetector.detectSubMatrix(RadarImage.getNoisyRadarImage(),
                            SpaceInvaders.getInvaderRadarImageB(), Integer.valueOf(input));
                    System.out.println("\nDetecting Type A and Type B Invaders " + "above " + input + " % . . .");
                    System.out.println("\n" + typeAMatches + " type A and " + typeBMatches + " type B invaders found.");
                } else {
                    System.out.println("Incorrect input. Input must be a number between 0 and 100 or \"exit\" to close " +
                            "the detector.");
                }
            } catch (NumberFormatException nfe) {
                System.out.println("Input must be a number or \"exit\"");
            } catch (NoSuchElementException nsee) {
                System.out.println("No more line found");
                break;
            } catch (IllegalStateException ise) {
                System.out.println("Scanner not available.");
                break;
            }
        }
    }
}