package com.zsoltfulop.utils;

/**
 * Formats and prints a complete matrix or one of its rows.
 */
public class MatrixPrinter {

    /**
     * @param matrix the matrix to be formatted and printed
     */
    public static void printMatrix(char[][] matrix) {
        for (char[] aMatrix : matrix) {
            // for each column
            for (char anAMatrix : aMatrix) {
                System.out.print(anAMatrix);
            }
            System.out.print("\n");
        }
    }

    /**
     * @param matrixRow the matrix row to be formatted and printed
     */
    public static void printMatrixRow(char[] matrixRow) {
        for (char aMatrixRow : matrixRow) {
            System.out.print(aMatrixRow);
        }
        System.out.print(" ");
    }
}
