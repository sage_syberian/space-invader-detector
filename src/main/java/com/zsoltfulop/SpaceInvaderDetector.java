package com.zsoltfulop;

import com.zsoltfulop.managers.TerminalManager;

/**
 * Application that detects Space Invaders in a noisy radar image.
 */
public class SpaceInvaderDetector {

    public static void main(String[] args) {

        TerminalManager.start();
    }
}
