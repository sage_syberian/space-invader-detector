package com.zsoltfulop.tools;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class MatrixComparatorTest {

    private static final char[][] MATRIX = new char[][]{
            "o-".toCharArray(),
            "oo".toCharArray()
    };

    private static final char[][] SUB_MATRIX = new char[][]{
            "oo".toCharArray(),
            "oo".toCharArray()
    };

    @Test
    public void measureSimilarityAt75Percentage() throws Exception {
        boolean similarity = MatrixComparator.measureSimilarity(
                MATRIX, SUB_MATRIX, 0.75f, 4);

        Assert.assertEquals(true, similarity);
    }

    @Test
    public void measureSimilarityAt76Percentage() throws Exception {
        boolean similarity = MatrixComparator.measureSimilarity(
                MATRIX, SUB_MATRIX, 0.76f, 4);

        Assert.assertEquals(false, similarity);
    }

    @Test
    public void calculateMaxCharMatch() throws Exception {
        Assert.assertEquals(4f, MatrixComparator.calculateMaxCharMatch(SUB_MATRIX), 0f);
        Assert.assertEquals(3f, MatrixComparator.calculateMaxCharMatch(MATRIX), 0f);
    }
}