package com.zsoltfulop.tools;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class StepIteratorTest {

    private static final char[][] MATRIX = new char[][]{
            "o-".toCharArray(),
            "oo".toCharArray()
    };

    private static final char[][] SUB_MATRIX = new char[][]{
            "oo".toCharArray(),
            "oo".toCharArray()
    };

    // Defining which iterations should contain which characters of the received matrices
    private char[][] subMatrixIteration1379 = new char[][]{"o".toCharArray()};
    private char[][] subMatrixIteration28 = new char[][]{"oo".toCharArray()};
    private char[][] subMatrixIteration46 = new char[][]{
            "o".toCharArray(),
            "o".toCharArray()
    };
    private char[][] subMatrixIteration5 = new char[][]{
            "oo".toCharArray(),
            "oo".toCharArray()
    };
    private char[][] matrixIteration179 = new char[][]{"o".toCharArray()};
    private char[][] matrixIteration2 = new char[][]{"o-".toCharArray()};
    private char[][] matrixIteration3 = new char[][]{"-".toCharArray()};
    private char[][] matrixIteration4 = new char[][]{
            "o".toCharArray(),
            "o".toCharArray()
    };
    private char[][] matrixIteration5 = new char[][]{
            "o-".toCharArray(),
            "oo".toCharArray()
    };
    private char[][] matrixIteration6 = new char[][]{
            "-".toCharArray(),
            "o".toCharArray()
    };
    private char[][] matrixIteration8 = new char[][]{"oo".toCharArray()};

    private static List<char[][]> expectedPairedSelectorWindowList = new ArrayList<char[][]>();

    @Before
    public void setUp() {
        //expected iteration 1
        expectedPairedSelectorWindowList.add(matrixIteration179);
        expectedPairedSelectorWindowList.add(subMatrixIteration1379);
        //expected iteration 2
        expectedPairedSelectorWindowList.add(matrixIteration2);
        expectedPairedSelectorWindowList.add(subMatrixIteration28);
        //expected iteration 3
        expectedPairedSelectorWindowList.add(matrixIteration3);
        expectedPairedSelectorWindowList.add(subMatrixIteration1379);
        //expected iteration 4
        expectedPairedSelectorWindowList.add(matrixIteration4);
        expectedPairedSelectorWindowList.add(subMatrixIteration46);
        //expected iteration 5
        expectedPairedSelectorWindowList.add(matrixIteration5);
        expectedPairedSelectorWindowList.add(subMatrixIteration5);
        //expected iteration 6
        expectedPairedSelectorWindowList.add(matrixIteration6);
        expectedPairedSelectorWindowList.add(subMatrixIteration46);
        //expected iteration 7
        expectedPairedSelectorWindowList.add(matrixIteration179);
        expectedPairedSelectorWindowList.add(subMatrixIteration1379);
        //expected iteration 8
        expectedPairedSelectorWindowList.add(matrixIteration8);
        expectedPairedSelectorWindowList.add(subMatrixIteration28);
        //expected iteration 9
        expectedPairedSelectorWindowList.add(matrixIteration179);
        expectedPairedSelectorWindowList.add(subMatrixIteration1379);
    }

    @Test
    public void iterateSameSize() throws Exception {
        StepIterator stepIterator = new StepIterator();

        List<char[][]> pairedSelectorWindowList = stepIterator.iterate(MATRIX, SUB_MATRIX);

        Assert.assertEquals(expectedPairedSelectorWindowList.size(), pairedSelectorWindowList.size());
        Assert.assertArrayEquals(expectedPairedSelectorWindowList.get(0), (pairedSelectorWindowList.get(0)));
        Assert.assertArrayEquals(expectedPairedSelectorWindowList.get(1), (pairedSelectorWindowList.get(1)));
        Assert.assertArrayEquals(expectedPairedSelectorWindowList.get(2), (pairedSelectorWindowList.get(2)));
        Assert.assertArrayEquals(expectedPairedSelectorWindowList.get(3), (pairedSelectorWindowList.get(3)));
        Assert.assertArrayEquals(expectedPairedSelectorWindowList.get(4), (pairedSelectorWindowList.get(4)));
        Assert.assertArrayEquals(expectedPairedSelectorWindowList.get(5), (pairedSelectorWindowList.get(5)));
        Assert.assertArrayEquals(expectedPairedSelectorWindowList.get(6), (pairedSelectorWindowList.get(6)));
        Assert.assertArrayEquals(expectedPairedSelectorWindowList.get(7), (pairedSelectorWindowList.get(7)));
        Assert.assertArrayEquals(expectedPairedSelectorWindowList.get(8), (pairedSelectorWindowList.get(8)));
        Assert.assertArrayEquals(expectedPairedSelectorWindowList.get(9), (pairedSelectorWindowList.get(9)));
        Assert.assertArrayEquals(expectedPairedSelectorWindowList.get(10), (pairedSelectorWindowList.get(10)));
        Assert.assertArrayEquals(expectedPairedSelectorWindowList.get(11), (pairedSelectorWindowList.get(11)));
        Assert.assertArrayEquals(expectedPairedSelectorWindowList.get(12), (pairedSelectorWindowList.get(12)));
        Assert.assertArrayEquals(expectedPairedSelectorWindowList.get(13), (pairedSelectorWindowList.get(13)));
        Assert.assertArrayEquals(expectedPairedSelectorWindowList.get(14), (pairedSelectorWindowList.get(14)));
        Assert.assertArrayEquals(expectedPairedSelectorWindowList.get(15), (pairedSelectorWindowList.get(15)));
        Assert.assertArrayEquals(expectedPairedSelectorWindowList.get(16), (pairedSelectorWindowList.get(16)));
        Assert.assertArrayEquals(expectedPairedSelectorWindowList.get(17), (pairedSelectorWindowList.get(17)));
    }

}