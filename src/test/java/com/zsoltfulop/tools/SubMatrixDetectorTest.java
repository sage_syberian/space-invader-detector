package com.zsoltfulop.tools;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({MatrixComparator.class, StepIterator.class})
public class SubMatrixDetectorTest {

    private StepIterator stepIteratorSpy;
    private List<char[][]> pairedSelectorWindowList;

    private static final char[][] MATRIX = new char[][]{
            "o-".toCharArray(),
            "oo".toCharArray()
    };

    private static final char[][] SUB_MATRIX = new char[][]{
            "oo".toCharArray(),
            "oo".toCharArray()
    };

    @Before
    public void setUp() throws Exception {
        pairedSelectorWindowList = new ArrayList<char[][]>();
        pairedSelectorWindowList.add(MATRIX);
        pairedSelectorWindowList.add(SUB_MATRIX);

        PowerMockito.mockStatic(MatrixComparator.class);
        stepIteratorSpy = PowerMockito.spy(new StepIterator());

        when(MatrixComparator.calculateMaxCharMatch(eq(SUB_MATRIX))).thenReturn(1f);

        when(stepIteratorSpy.iterate(MATRIX, SUB_MATRIX)).thenReturn(pairedSelectorWindowList);
        doNothing().when(stepIteratorSpy, "stepIterateRow", eq(MATRIX), eq(SUB_MATRIX));
        doNothing().when(stepIteratorSpy, "stepIterateCol", any(char[][].class), any(char[][].class));
    }

    @Test
    public void iterateMaxMatch() throws Exception {
        when(MatrixComparator.measureSimilarity(
                any(char[][].class), any(char[][].class), eq(0.75f), eq(1f))).thenReturn(true);

        int matches = SubMatrixDetector.detectSubMatrix(MATRIX, SUB_MATRIX, 75);

        PowerMockito.verifyStatic(MatrixComparator.class, times(1));
        MatrixComparator.calculateMaxCharMatch(any(char[][].class));

        // In the case of 2 2x2 matrices 9 selector windows are generated, so measureSimilarity method is called
        // 9 times. Since it returns always true by the mock configuration, detectSubMatrix returns 9 matches.
        PowerMockito.verifyStatic(MatrixComparator.class, times(9));
        MatrixComparator.measureSimilarity(any(char[][].class), any(char[][].class), eq(0.75f), eq(1f));

        Mockito.verify(stepIteratorSpy, times(1))
                .iterate(eq(MATRIX), eq(SUB_MATRIX));

        Assert.assertEquals(9, matches);
    }

    @Test
    public void iterateNoMatch() throws Exception {
        when(MatrixComparator.measureSimilarity(
                any(char[][].class), any(char[][].class), eq(0.75f), eq(1f))).thenReturn(false);

        int matches = SubMatrixDetector.detectSubMatrix(MATRIX, SUB_MATRIX, 75);

        PowerMockito.verifyStatic(MatrixComparator.class, times(1));
        MatrixComparator.calculateMaxCharMatch(any(char[][].class));

        //  measureSimilarity returns always false by the mock configuration, detectSubMatrix returns 0 match.
        PowerMockito.verifyStatic(MatrixComparator.class, times(9));
        MatrixComparator.measureSimilarity(any(char[][].class), any(char[][].class), eq(0.75f), eq(1f));

        Mockito.verify(stepIteratorSpy, times(1))
                .iterate(eq(MATRIX), eq(SUB_MATRIX));

        Assert.assertEquals(0, matches);
    }
}