package com.zsoltfulop.managers;

import com.zsoltfulop.managers.TerminalManager;
import com.zsoltfulop.tools.SubMatrixDetector;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SubMatrixDetector.class)
public class TerminalManagerTest {

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(SubMatrixDetector.class);
        PowerMockito.when(SubMatrixDetector.class,
                SubMatrixDetector.detectSubMatrix(
                        any(char[][].class), any(char[][].class), anyInt())).thenReturn(1);
    }

    @Test
    public void startWithExit() throws Exception {
        String input = "exit";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        TerminalManager.start();

        String outString = out.toString();

        PowerMockito.verifyStatic(SubMatrixDetector.class, times(0));
        SubMatrixDetector.detectSubMatrix(any(char[][].class), any(char[][].class), anyInt());

        Assert.assertTrue(outString.contains("Thank you for using the radar enhancement module. See you next time!"));
        Assert.assertFalse(outString.contains("Input must be a number or \"exit\""));
        Assert.assertFalse(outString.contains("invaders found."));
        Assert.assertFalse(outString.contains("Detecting Type A and Type B Invaders"));
        Assert.assertFalse(outString.contains("Incorrect input."));
    }

    @Test
    public void startWithValidPercentage() throws Exception {
        String input = "70";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        TerminalManager.start();

        PowerMockito.verifyStatic(SubMatrixDetector.class, times(2));
        SubMatrixDetector.detectSubMatrix(any(char[][].class), any(char[][].class), anyInt());

        String outString = out.toString();

        Assert.assertTrue(outString.contains("Detecting Type A and Type B Invaders"));
        Assert.assertTrue(outString.contains("1 type A and 1 type B invaders found."));
        Assert.assertTrue(outString.contains("Detecting Type A and Type B Invaders"));
        Assert.assertFalse(outString.contains("Thank you for using the radar enhancement module. See you next time!"));
        Assert.assertFalse(outString.contains("Input must be a number or \"exit\""));
    }

    @Test
    public void startWithInvalidPercentage() throws Exception {
        String input = "200";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        TerminalManager.start();

        String outString = out.toString();

        PowerMockito.verifyStatic(SubMatrixDetector.class, times(0));
        SubMatrixDetector.detectSubMatrix(any(char[][].class), any(char[][].class), anyInt());

        Assert.assertTrue(outString.contains("No more line found"));
        Assert.assertTrue(outString.contains("Incorrect input"));
        Assert.assertFalse(outString.contains("Input must be a number or \"exit\""));
        Assert.assertFalse(outString.contains("invaders found."));
        Assert.assertFalse(outString.contains("Detecting Type A and Type B Invaders"));
        Assert.assertFalse(outString.contains("Thank you for using the radar enhancement module. See you next time!"));
    }

    @Test
    public void startWithInvalidString() throws Exception {
        String input = "abcd";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));

        TerminalManager.start();

        String outString = out.toString();

        PowerMockito.verifyStatic(SubMatrixDetector.class, times(0));
        SubMatrixDetector.detectSubMatrix(any(char[][].class), any(char[][].class), anyInt());

        Assert.assertTrue(outString.contains("Input must be a number or \"exit\""));
        Assert.assertTrue(outString.contains("No more line found"));
        Assert.assertFalse(outString.contains("Incorrect input."));
        Assert.assertFalse(outString.contains("invaders found."));
        Assert.assertFalse(outString.contains("Detecting Type A and Type B Invaders"));
        Assert.assertFalse(outString.contains("Thank you for using the radar enhancement module. See you next time!"));
    }
}